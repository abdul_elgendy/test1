Calibrations='Module_1_polyspace_defect_drs_template.xml';
%insert calibration file
%
xDoc = xmlread(Calibrations);
globa=xDoc.getElementsByTagName('global');
files = globa.item(0).getElementsByTagName('file');
num_files = files.getLength;
smallSubstring = 'K_';

for i=0:(num_files-1)
    
    scalar=files.item(i).getElementsByTagName('scalar');
    array=files.item(i).getElementsByTagName('array');
    struct=files.item(i).getElementsByTagName('struct');
    
    num_scalar = scalar.getLength;
    num_array = array.getLength;
    num_struct = struct.getLength;
    
    for j = 0:(num_scalar - 1)
        scalar.item(j).getAttribute('name');
        largeString = char(scalar.item(j).getAttribute('name'));
        Check = contains(largeString, smallSubstring);
        if (Check == 1)
            Assign_Attributes(scalar.item(j));
        end
    end
   
    for j=0:(num_array-1) 
        largeString = char(array.item(j).getAttribute('name'));
        Check = contains(largeString, smallSubstring);
        if(Check == 1) 
            Array_Or_Struct_Traversal(array.item(j));
        end
    end
    
    for j=0:(num_struct-1) 
        largeString = char(struct.item(j).getAttribute('name'));
        Check = contains(largeString, smallSubstring);
        if(Check == 1) 
            Array_Or_Struct_Traversal(struct.item(j));
        end
    end
end
%%%
 %%
%%Assign changes to XML file
xmlwrite('Module_1_polyspace_defect_drs_template.xml',xDoc); 